# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_210_307_090_843) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "nodes", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.integer "parent_id"
    t.integer "lft"
    t.integer "rgt"
    t.integer "depth"
    t.string "locale", limit: 6
    t.string "content_type"
    t.integer "content_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "item_position"
    t.boolean "active", default: true, null: false
    t.index ["active"], name: "index_nodes_on_active"
    t.index %w[content_type content_id], name: "index_nodes_on_content_type_and_content_id"
    t.index ["depth"], name: "index_nodes_on_depth"
    t.index ["lft"], name: "index_nodes_on_lft"
    t.index ["locale"], name: "index_nodes_on_locale"
    t.index ["name"], name: "index_nodes_on_name"
    t.index ["parent_id"], name: "index_nodes_on_parent_id"
    t.index ["rgt"], name: "index_nodes_on_rgt"
    t.index ["slug"], name: "index_nodes_on_slug"
  end

  create_table "releaf_i18n_entries", id: :serial, force: :cascade do |t|
    t.string "key", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_releaf_i18n_entries_on_key"
  end

  create_table "releaf_i18n_entry_translations", id: :serial, force: :cascade do |t|
    t.integer "i18n_entry_id", null: false
    t.string "locale", limit: 5, null: false
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["i18n_entry_id"], name: "index_releaf_i18n_entry_translations_on_i18n_entry_id"
    t.index %w[locale i18n_entry_id], name: "index_releaf_i18n_entry_translations_on_locale_i18n_entry_id",
                                      unique: true
    t.index ["locale"], name: "index_releaf_i18n_entry_translations_on_locale"
  end

  create_table "releaf_permissions", id: :serial, force: :cascade do |t|
    t.integer "owner_id"
    t.string "owner_type"
    t.string "permission"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index %w[owner_id owner_type], name: "index_releaf_permissions_on_owner_id_and_owner_type"
    t.index ["permission"], name: "index_releaf_permissions_on_permission"
  end

  create_table "releaf_richtext_attachments", id: :serial, force: :cascade do |t|
    t.string "file_uid"
    t.string "file_name"
    t.string "file_type"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "releaf_roles", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.string "default_controller"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "releaf_users", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.string "surname", null: false
    t.string "locale"
    t.integer "role_id"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_releaf_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_releaf_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_releaf_users_on_role_id"
  end

  create_table "settings", id: :serial, force: :cascade do |t|
    t.string "var", null: false
    t.text "value"
    t.integer "thing_id"
    t.string "thing_type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index %w[thing_type thing_id var], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true
  end
end
