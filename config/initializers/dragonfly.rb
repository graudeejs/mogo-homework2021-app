# frozen_string_literal: true

require "dragonfly"

# Configure
Dragonfly.app.configure do
  plugin :imagemagick

  secret "8f0b71273dd5a5f3d8dadfabe4f1530d1db5b9338b058c9c0f19bbff1c4cb988"

  url_format "/media/:job/:name"

  datastore :file,
            root_path: Rails.root.join("public/system/dragonfly", Rails.env),
            server_root: Rails.root.join("public")
end

# Logger
Dragonfly.logger = Rails.logger

# Mount as middleware
Rails.application.middleware.use Dragonfly::Middleware

# Add model functionality
ActiveSupport.on_load(:active_record) do
  extend Dragonfly::Model
  extend Dragonfly::Model::Validations
end
