# frozen_string_literal: true

class Node < ActiveRecord::Base
  include Releaf::Content::Node
end
